FROM opensuse/tumbleweed

RUN zypper --non-interactive ar -f http://download.opensuse.org/repositories/Documentation:Tools/openSUSE_Tumbleweed Documentation:Tools

RUN zypper --no-gpg-checks in --recommends --from Documentation:Tools -y daps
RUN zypper --no-gpg-checks in -y tar

RUN mkdir /project
WORKDIR /project

